import os
import sys
import multiprocessing
import getopt
import math
from collections import deque

import cv2
from pywt import *
from cv2 import imread,imwrite,line
import numpy as np
import pytesseract

def conditional_create(*Dirs):
  for Dir in Dirs:
    if not os.path.exists(Dir):
      os.makedirs(Dir)

# bfs-based approach for segmentaion, could be
# used with a potential function, but it's in practice
# only to find connex regions
def run_around(img,start,mask=None,POTENTIAL=10):
  if mask == None:
    mask = np.zeros(img.shape,dtype=np.uint8)
  mask[tuple(start)] = POTENTIAL
  Q = deque()
  Q.append(start)
  x,y = start
  while Q:
    x,y = Q.popleft()
    for dx,dy in [(1,0),(-1,0),(0,-1),(0,1)]:
      diff = abs(int(img[x+dx][y+dy]) - int(img[x][y]))
      if mask[x+dx][y+dy] < mask[x][y] and diff < mask[x][y] and x+dx > 0 and x + dx < img.shape[0]-1 and y+dy > 0 and y+dy < img.shape[1]-1:
        Q.append((x+dx,y+dy))
        mask[x+dx][y+dy] = mask[x][y] - diff
  mask[mask>0] = 255
  return mask

def locate_image(info):
  filename,label_file,index,debug,progress,THRESHOLD_MAGIC_NUMBER = info
  print filename
  if THRESHOLD_MAGIC_NUMBER < 10:
    return -3
  oimg = imread(filename,0)
  label = imread(label_file,0)

  # Section 1
  if progress:
    sys.stderr.write("Doing %d\n"%index)

  def intensity_correction(img):
    #hard cap 15 here, for very bright images
    avg = max(1.5*int(np.average(img)),15)
    img = cv2.blur(img,(3,3))
    img[img>avg] = avg
    img *= np.uint8(255/avg)
    return img

  img = intensity_correction(oimg.copy())

  if debug: imwrite('threshed/%03d.jpg'%index,img)
  LL,(LH,HL,HH) = dwt2(img,'bior1.5')

  def extract_height(axis):
    base = HL if axis == 'h' else np.transpose(LH)
    heights = []
    # scan vertical lines
    for x in xrange(0,base.shape[0]):
      height = 0
      for y in xrange(1,base.shape[1]):
        if abs(base[x][y-1]-base[x][y]) > THRESHOLD_MAGIC_NUMBER:
          height += 1
      if height < 100 and height> 0:
        heights.append((height,x))
    return heights

  def compute_normalized(axis='h'):
    heights = extract_height(axis)
    base = HL if axis == 'h' else np.transpose(LH)

    retimg = np.zeros(base.shape,dtype=np.uint8)
    avg = sum(map(lambda x:x[0],heights))/float(len(heights))
    acceptable = map(lambda x: x[1],heights)
    if debug:
      not_acceptable = map(lambda x: x[1],filter(lambda x: x[0] <= avg,heights))
  
    for x in acceptable:
      for y in xrange(1,base.shape[1]):
        if abs(base[x][y-1]-base[x][y]) > THRESHOLD_MAGIC_NUMBER:
          retimg[x][y] = 255
    if axis != 'h': retimg = np.transpose(retimg)
    return retimg

  # Section 2
  # This is equivalent to the step2 on the reference article, it extracts
  # the heights(number of high frequency transitions above a certain treshold.
  # The points are then binary tresholded.
  step2_img = compute_normalized();
  if debug:
    imwrite("step2/%03d.jpg"%index,step2_img)
    imwrite("horizontal/%03d.jpg"%index,step2_img)
    imwrite("vertical/%03d.jpg"%index,compute_normalized('v'))

  # Section 3
  # repeatedly select points that have more than 1/4s of the neighbors set
  img = step2_img
  for i in xrange(20):
    img = cv2.blur(img,(5,5))
    img[img>255/4] = 255
    img = cv2.blur(img,(3,3))
    img[img < 3*255/4] = 0 
  
  # Do an opening to remove residual prongs
  kernel = np.ones((7,7),dtype=np.uint8)
  img = cv2.morphologyEx(img,cv2.MORPH_OPEN,kernel)

  if debug:
    guessing = img.copy()
    imwrite("guessing/%03d.jpg"%index,img)


  # generate a sorted list of all the contours, with (contour area, bounding
  # rectangle, contour) as items.
  contours = sorted(map(lambda x: (cv2.contourArea(x),cv2.boundingRect(x),x),
              cv2.findContours(img,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)[0]),
              key=lambda x: x[0])[::-1]

  # Fint the center of mass of the region. That's our target point
  def get_cm(contour):
    M = cv2.moments(contour)
    return (2*int(M['m01']/M['m00']),2*int(M['m10']/M['m00']))

  mask = np.zeros(oimg.shape)
  # Section 4
  # Look ultil we find the contour with the biggest area that's slightly 
  # rectanglish
  def greyfy(img):
    minval = img.min()
    maxval = img.max()
    img = 255*(img-minval)/(maxval-minval)
    return img.astype(np.uint8)
  mask = np.zeros(img.shape)
  base_run = cv2.Canny(oimg.copy(),40,100)
  base_run = cv2.morphologyEx(step2_img.copy() ,cv2.MORPH_OPEN,(7,7))
  base_run = cv2.blur(base_run,(5,5))
  base_run[base_run>0] = 255
  base_run = cv2.morphologyEx(base_run ,cv2.MORPH_CLOSE,(9,9))
  base_run[base_run==255] = 127
  base_run[base_run==0] = 255
  base_run[base_run==127] = 0
  for contour_triple in contours:
    area, rect, contour = contour_triple
    start = get_cm(contour)
    if rect[2] > 1.5*rect[3]:
      biggest_contour = contour
      # to extract a good mask, we first try to find the connected region
      try:
        mask = run_around(base_run,(start[0]/2,start[1]/2))
      except:
        mask = np.zeros(img.shape)
      imwrite("base_run/%s.jpg"%filename.split("/")[-1].split(".")[0],mask)
      for _ in xrange(10):
        mask = cv2.blur(mask,(5,5))
        mask[mask>255/4] = 255
        mask = cv2.blur(mask,(3,3))
        mask[mask < 3*255/4] = 0 
      mask_contour = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)[0][0]
      x,y,w,h = cv2.boundingRect(mask_contour)
      x -= 5
      y -= 5
      w += 5
      h += 5
      mask = oimg[2*y:2*(y+h),2*x:2*(x+w)]
      if debug:
        imwrite("mask/%s.jpg"%(filename.split("/")[-1].split(".")[0]),mask)
        try:
          not_mask = oimg.copy()
          not_mask[2*y:2*(y+h),2*x:2*(x+w)] = np.uint8(np.ones((2*h,2*w))*255)
          imwrite("not_mask/%s.jpg"%(filename.split("/")[-1].split(".")[0]),not_mask)
        except:
          pass
      break

  try: 
    cm = get_cm(biggest_contour)

    # mark the points for debug
    if debug:
      #print cm
      fname = filename.split("/")
      fname[1] = 'label'
      res = cv2.resize(oimg.copy(),(oimg.shape[1]/2,oimg.shape[0]/2))
    
    # If our candidate point, the cm, in in a white area of the image, we
    # consider it a success
    if label[cm] != 0:
      if debug:
        print 'yes'
        resized_oimg= cv2.resize(oimg.copy(),(img.shape[1],img.shape[0]))
        imwrite('finished.png',np.multiply(resized_oimg,(img/255)))
      ans = 0
    else:
      #print 'Missed'
      resized_label = cv2.resize(label.copy(),(img.shape[1],img.shape[0]))
      res = np.multiply(img,resized_label)
      if res.any():
        ans = -1
        if progress: print 'NORIGHT: Candidate wrong'
      else:
        ans = -2
        if progress: print 'NOCAND: The plate isn\'t on any candidate'

      if debug:
        #plate_contour = cv2.findContours(label,
        #                                  cv2.RETR_EXTERNAL,
        #                                  cv2.CHAIN_APPROX_NONE)[0]
        #plate_cm = get_cm(plate_contour)
        imwrite("missed/%s.jpg"%filename.split("/")[-1].split(".")[0],step2_img)
        imwrite("missed/%s.2.jpg"%filename.split("/")[-1].split(".")[0],guessing)

    if debug:
      cv2.drawContours(res,[biggest_contour],-1,(255,255,255))
      #cv2.circle(res,(cm[1]/2,cm[0]/2),5,(255,0,0))
      res = cv2.resize(res,(oimg.shape[1],oimg.shape[0]))
  #In these cases, no such contour was found.
  except (UnboundLocalError,IndexError):
    if debug:
      imwrite("problematic/%s.jpg"%filename.split("/")[-1].split(".")[0],step2_img)
      imwrite("problematic/%s.2.jpg"%filename.split("/")[-1].split(".")[0],guessing)
    if progress: print 'NOPLATE: Nothing looks like a plate'
    ans = -3
    res = img
    ans = locate_image([filename,label_file,index,debug,progress,THRESHOLD_MAGIC_NUMBER-1])

  if debug: imwrite("answers/%s.jpg"%filename.split("/")[-1].split(".")[0],res)

  return [ans,mask]

def sum_eq(v,b):
  return sum(map(lambda x: x == b,v))

def generate_graph(info):
  import matplotlib.pyplot as plt
  #from scipy.interpolate import spline
  errors = []
  basis = xrange(5,30,2)
  pool = multiprocessing.Pool(processes=7)
  for i in basis:
    print "%.2f"%(float(i-5)/(30-5))
    cases = pool.map(locate_image,map(lambda x: x+[i],info))
    errors.append(map(lambda x: sum_eq(cases,x),[-1,-2,-3]))

  #change from a vector of triples to three vectors
  tot = map(sum,errors)
  errors = zip(*errors)+[tot]
  labels = ['1: Wrong candidate',
            '2: No valid candidate',
            '3: No candidate',
            '4: Total Error']
  style = ['k--','k:','k-.','k']
  for i,error in enumerate(errors):
    plt.plot(basis,error,label=labels[i])
  plt.xlabel('Threshold')
  plt.ylabel('Number of errors')
  plt.title('Errors X Tresholds')
  plt.legend(loc='upper center',shadow = False, fontsize = 'large')
  plt.savefig('tresholds.png')

def box_to_contour(box):
  pt     = np.zeros((4,2))
  _angle = box[2]*math.pi/180.0
  b      = math.cos(_angle)*0.5
  a      = math.sin(_angle)*0.5
  center = box[0]
  dims   = box[1]

  pt[0][0] = center[0] - a*dims[1] - b*dims[0]
  pt[0][1] = center[1] + b*dims[1] - a*dims[0]
  pt[1][0] = center[0] + a*dims[1] - b*dims[0]
  pt[1][1] = center[1] - b*dims[1] - a*dims[0]
  pt[2][0] = 2*center[0] - pt[0][0]
  pt[2][1] = 2*center[1] - pt[0][1]
  pt[3][0] = 2*center[0] - pt[1][0]
  pt[3][1] = 2*center[1] - pt[1][1]
  return pt

def longest_run(column):
  max_run = 0
  current_run = 0
  for l in column:
    if l:
      current_run += 1
    else:
      if current_run > max_run:
        max_run = current_run
      current_run = 0
  if current_run > max_run:
    max_run = current_run
  return max_run

def count_column(image):
  return sum([longest_run(c) for c in image.T])

def find_transform(image):
  corner_mask = map(lambda x:run_around(image,x),((48,148),(48,2),(2,2),(2,148)))
  acc_mask = sum(corner_mask)
  acc_mask[acc_mask>0]=1
  image[image>0] = 1
  image = image+acc_mask
  image[image>0] = 255
  image = cv2.dilate(image,(5,5))
  stats = []
    #M = cv2.getPerspectiveTransform(
    #     np.float32(np.array(((150,50),(i,50),(0,0),(150,0)))),
    #     np.float32(np.array(((150,50),(0,50),(0,0),(150,0)))))
    #projected = cv2.warpPerspective(image,M,(150,50))
    #stats.append((count_column(projected),i,projected))
  for i in range(0,21,4):
    for j in range(0,21,4):
      for k in range(0,21,4): #for l in range(0,11,2):
        for l in range(0,21,4): #for l in range(0,11,2):
          M = cv2.getPerspectiveTransform(
               np.float32(np.array(((150+j,50),(-i,50),(-k,0),(150+l,0)))),
               np.float32(np.array(((150,50),(0,50),(0,0),(150,0)))))
          projected = cv2.warpPerspective(image,M,(150,50),borderValue=255)
          stats.append((count_column(projected),(i,j,k,l),projected))
  return max(stats)[2]

# extract edges from the original image, using canny
def extract_edges(image,n,debug):
  original = image.copy()
  image = cv2.dilate(image,(5,5))

  def normalize(image):
    max_i = image[:].max()
    min_i = image[:].min()
    return np.uint8((image-min_i)*256.0/(max_i-min_i))
  image = normalize(image)
  image = cv2.GaussianBlur(image,(5,5),0)
  image = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                                  cv2.THRESH_BINARY,9,-2)
  size = np.array(image.shape)
  mask = np.zeros(size+np.array([10,10]))
  mask[5:size[0]+5,5:size[1]+5] = image

  ksiz = 5
  for i in xrange(100):
    mask = cv2.blur(mask,(ksiz,ksiz))
    mask[mask>255/4] = 255
    mask[mask<255/4] = 0
    mask = cv2.blur(mask,(ksiz,ksiz))
    mask[mask > 3*255/4] = 255
    mask[mask < 3*255/4] = 0 

  # get the one with biggest area
  mask = np.uint8(mask)
  mask_contour = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)[0][0]
  box = cv2.minAreaRect(mask_contour)
  box = np.int0(box_to_contour(box))
  cv2.drawContours(image,[box],0,(255),2)
  M = cv2.getPerspectiveTransform(
       np.float32(box),
       np.float32(np.array(((150,50),(0,50),(0,0),(150,0)))))

  image = cv2.warpPerspective(original,M,(150,50))
  _,edges= cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
  edges = find_transform(edges)

  if debug:
    imwrite('edges/%d.png'%n,edges)
    imwrite('stepped/%d.png'%n,image)
  return edges

def separate_letters(image):
  # get a histogram with the height(number of white pixels) of columns
  height_histogram = np.array([ longest_run(c) for c in image.T ])
  # split the histogram to find the walls of the letters
  average = sum(height_histogram)/len(height_histogram)
  height_histogram[height_histogram<average] = 0
  height_histogram[height_histogram>average] = 1
  return height_histogram

from pytesseract import image_to_string
from PIL import Image
def segment_letters(image,n,debug):
  horizontal = separate_letters(image)
  vertical = separate_letters(image.T)

  #find the horizontal line with the most masks
  masks = []
  start,midh = 0,image.shape[0]/2
  for i,p in enumerate(horizontal):
    if p:
      dx = i-1-start
      if dx > 3:
        lbound,ubound = 0,0
        while not vertical[midh-lbound] and lbound < midh:
          lbound += 1

        while not vertical[midh+ubound] and ubound < image.shape[0]-midh-1:
          ubound += 1
        dy = ubound+lbound
        if dy > 3:
          base = np.ones((dy+6,dx+6))*255
          base[3:dy+3,3:dx+3] = image[midh-lbound:midh+ubound,start:i-1]
          masks.append(np.uint8(base))
      start = i

  letters = []
  for i,image in enumerate(masks):
    letters.append(image_to_string(Image.fromarray(image),config='-psm 10 -l eng'))
    conditional_create('letters/%03d')
    if debug: imwrite('letters/%03d/%02d.png'%(n,i),image)
  print "%d: %s"%(n,''.join(letters))

def process(info):
  index,debug = info[2:4]
  result,segmented = locate_image(info)
  if result or not segmented.any():
    return result
  try:
    edges = extract_edges(segmented,index,debug)
    letters = segment_letters(edges,index,debug)
  except:
    pass

if __name__ == '__main__':

  ops = dict(getopt.getopt(sys.argv[1:],'s:d:hgj:PT:G')[0])

  if '-h' in ops:
    print "Usage: %s [opts]"%sys.argv[0]
    print "\t-s source directory, defaults to LicensePlates"
    print "\t\t<source dir> has orig and label subfolders, default: LicensePlates"
    print "\t-d answers directory, defaults to answers"
    print "\t-g debug flag(Warning: Many many files)"
    print "\t-p print progress flag"
    print "\t-j number of process, requires -P no make sense"
    print "\t-P run in parallel,order not guaranteed"
    print "\t-T Treshold(sensibility knob)"
    print "\t-G Generate graph with sensibilities"
    sys.exit(0)

  srcdir    = ops.get('-s','LicensePlates')+"/%s/%04d.pgm"
  dstdir    = ops.get('-d','answers')
  debug     = not ops.get('-g',True)
  progress  = not ops.get('-p',True)
  parallel  = not ops.get('-P',True)
  gen_graph = not ops.get('-G',True)
  processes = int(ops.get('-j','5'))
  treshold  = int(ops.get('-T','20'))

  if debug:
    conditional_create('problematic',
                        'missed',
                        'answers',
                        'letters',
                        'stepped',
                        'edges',
                        'not_mask',
                        'mask',
                        'step2',
                        'horizontal',
                        'vertical',
                        'threshed',
                        'base_run')

  cases = range(1,991)

  if gen_graph:
    print "Generating graph, this will take a while"
    generate_graph([[srcdir%('orig',idx),
                      srcdir%('label',idx),
                      idx,
                      debug,
                      progress] for idx in cases ])
    sys.exit(0)

  #hack due to python inability to serialize lambdas and closures
  if parallel:
    pool = multiprocessing.Pool(processes=processes)
    mapper = pool.map
  else:
    mapper = map
  cases = mapper(process,
                  [(srcdir%('orig',idx),
                    srcdir%('label',idx),
                    idx,
                    debug,
                    progress,
                    treshold) for idx in cases ])


  print "Got %.2f cases right"%(float(sum_eq(cases,0))/len(cases))
  print "In %d cases the plate was eliminated"%sum_eq(cases,-3)
  print "In %d cases the candidate was not correctly found"%sum_eq(cases,-1)
  print "In %d cases no candidate contained the plate"%sum_eq(cases,-2)

