import cv2
import numpy as np
from PIL import Image,ImageDraw,ImageFont
import os
from sklearn.decomposition import PCA
import string

font_dir = "fonts"
rendered_dir = "rendered"
initial_size = 40
img_size = 320

delt_x_range = [-10,10]
delt_y_range = [0,10]

def find_coeffs(pb):
  pa = [(0,0),(0,img_size),(img_size,0),(img_size,img_size)]
  matrix = []
  for p1, p2 in zip(pa, pb):
    matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0]*p1[0], -p2[0]*p1[1]])
    matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1]*p1[0], -p2[1]*p1[1]])

  A = np.matrix(matrix, dtype=np.float)
  B = np.array(pb).reshape(8)

  res = np.dot(np.linalg.inv(A.T * A) * A.T, B)
  return np.array(res).reshape(8)

if not os.path.exists(rendered_dir):
  os.mkdir(rendered_dir)
  for glyph in string.ascii_uppercase + string.digits:
    os.mkdir(os.path.join(rendered_dir,glyph))

final_size = (60,33)
def generate_images():
  def draw_letter(shift,size):
    # letters are grey for background elimination
    image = Image.new("RGBA", (img_size,img_size), (255,255,255))
    usr_font = ImageFont.truetype(font_path, size)
    d_usr = ImageDraw.Draw(image)
    d_usr = d_usr.text((shift,shift),glyph,(128,128,128), font=usr_font)
    return image

  big_w,big_h = 0,0
  for font_file in os.listdir(font_dir):
    font_path = os.path.join(font_dir,font_file)
    font_name = font_file.split(".")[0]

    if os.path.isfile(font_path) and font_file[-3:] == "ttf":
      for glyph in string.ascii_uppercase + string.digits:
        print glyph
        # resized image
        for i in range(0,31,10):
          image = draw_letter((initial_size-40-i)/2+img_size/2-initial_size/2,40+i)
          #save( os.path.join(rendered_dir,glyph,font_name+".jpg"), "JPEG")
          for dx in range(-100,101,20):
            for dy in range(0,101,20):
              pb = [(-dy,dx),
                    (0,img_size),
                    (img_size,0),
                    (img_size+dy,img_size-dx)]
              coeffs = find_coeffs(pb)
              new_image = image.transform((img_size,img_size), Image.PERSPECTIVE, coeffs,
                                          Image.BICUBIC)
              # convert to numpy matrix
              matrix = np.array(new_image)
              matrix = matrix[:,:,1]
              # fix background
              matrix[matrix==0] = 255
              matrix[matrix<= 128] = 128
              matrix[:] -= 128
              matrix[:] *=2
  
              matrix = matrix.copy()
              matrix2 = matrix.copy()
  
              matrix2[matrix2>= 128] = 128
              matrix2[matrix2< 128] = 255
              matrix2[matrix2==128] = 0
              cnt,_= cv2.findContours(matrix2,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
              x,y,w,h = cv2.boundingRect(cnt[0])
              #cv2.rectangle(matrix,(x,y),(x+w,y+h),(0,0,0),2)
  
              h = min(h,60)
  
              final = np.ones(final_size)*255
              final[:h,:w] = matrix[y:y+h,x:x+w]
  
              big_w = max(big_w,w)
              big_h = max(big_h,h)
  
              # finish by toning contrast down
              for j in xrange(0,121,20):
                new_final = final.copy()
                new_final[new_final >= 255-j] = 255-j
                new_final += j/2
                yield new_final
                #cv2.imwrite(os.path.join(
                #    rendered_dir,glyph,"%s_%d_%d_%d_%d.jpg"%(font_name,i,dx,dy,j)),new_final)
  
  
              # fix letters
              #matrix[matrix<255] = 0
              #new_image = Image.fromarray(np.uint8(matrix))
              #new_image.putdata(matrix)
              #new_image.save(os.path.join(
              #    rendered_dir,glyph,"%s_%d_%d_%d.jpg"%(font_name,i,dx,dy)), "JPEG")
          
        # rotated image
        # blotched image
  print big_w,big_h

def generate_freqs():
  for image in generate_images():
    fft_img = np.fft.fft2(image)
    fft_indexes = fft_img.flatten()
    yield fft_indexes

pca=PCA()
pca.fit(generate_freqs())
print(pca.explained_variance_ratio_)
