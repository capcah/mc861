#include "ift.h"

int main(int argc, char* argv []) {

  if (argc < 4){
    iftError("Usage: <images folder> <labels folder> <output folder>", argv[0]);
  }

  iftDir* imgsDir = iftLoadFilesFromDirectory(argv[1], "pgm");
  iftDir* labelsDir = iftLoadFilesFromDirectory(argv[2], "pgm");

  iftImage* candImg;
  iftImage* labelImg;

  for (int i = 0; i <imgsDir->nfiles; ++i) {
    printf("Image: %s ", imgsDir->files[i]->pathname);
    labelImg = iftReadImageByExt(labelsDir->files[i]->pathname);
    origImg = iftReadImageByExt(imgsDir->files[i]->pathname);

    int numCandidates = iftMaximumValue(candImg);

    iftAdjRel *A = iftCircular(5.0);

    iftImageForest* forest = iftCreateImageForest(origImg,A);

    iftWaterGrayForest(fst,labelImg);
  }
}
